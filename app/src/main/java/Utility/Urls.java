package Utility;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by haider_kazal on 12/14/15.
 */
public class Urls {
    public static String[] Urls = {
            "http://api.ajkerdeal.com/CustomerAccess/Login/",
            "http://api.ajkerdeal.com/CustomerAccess/SignUp/",
            "http://api.ajkerdeal.com/CustomerAccess/ResetPassword/",
            "http://api.ajkerdeal.com/Banner/AppBanner/"
    };

    public static String[] TestUrls = {
            "http://192.168.0.87/CustomerAccess/Login/",
            "http://192.168.0.87/CustomerAccess/SignUp/",
            "http://192.168.0.87/CustomerAccess/ResetPassword/",
            "http://192.168.0.39/Banner/AppBanner/"
    };

    public static String getUrl(UrlType urlType) {
        int index = urlType.getValue();
        return Urls[index];
    }

    public static String getTestUrl(UrlType urlType) {
        int index = urlType.getValue();
        return TestUrls[index];
    }

    public enum UrlType {
        LOGIN(0),
        SIGNUP(1),
        RESET_PASSWORD(2),
        BANNER(3);

        private final int value;

        UrlType(int value) {
            this.value = value;
        }

        public int getValue() {
            return value;
        }
    };
}