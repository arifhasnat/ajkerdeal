package SharedPreference;

import android.content.Context;
import android.content.SharedPreferences;

import Models.UserDataModel;

/**
 * Created by Kazal on 16-Dec-15.
 */
public class UserInfo {
    private static UserInfo instance;
    private static final String SHARED_PREFERENCE_FILE_NAME = "UserInfo";
    private static SharedPreferences sharedPreferences;

    private UserInfo() {
    }

    public static UserInfo getInstance(final Context context) {
        UserInfo.sharedPreferences = context.getSharedPreferences(SHARED_PREFERENCE_FILE_NAME,
                Context.MODE_PRIVATE);
        if(instance == null) {
            instance = new UserInfo();
        }
        return instance;
    }

    public void storeUserInfo(UserDataModel data) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putInt("Id", data.Id);
        editor.putString("Name", data.Name);
        editor.putString("Email", data.Email);
        editor.putString("Mobile", data.Mobile);
        editor.putString("Address", data.Address);
        editor.putString("Gender", data.Gender);
        editor.putString("Location", data.Location);
        editor.putString("District", data.District);
        editor.apply();
    }

    public int getId() {
        return sharedPreferences.getInt("Id", 0);
    }

    public String getName() {
        return sharedPreferences.getString("Name", "");
    }

    public String getEmail() {
        return sharedPreferences.getString("Email", "");
    }

    public String getMobile() {
        return sharedPreferences.getString("Mobile", "");
    }

    public String getAddress() {
        return sharedPreferences.getString("Address", "");
    }

    public String getGender() {
        return sharedPreferences.getString("Gender", "");
    }

    public String getLocation() {
        return sharedPreferences.getString("Location", "");
    }

    public String getDistrict() {
        return sharedPreferences.getString("District", "");
    }
}
