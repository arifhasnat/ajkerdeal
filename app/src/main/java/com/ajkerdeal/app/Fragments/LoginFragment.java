package com.ajkerdeal.app.Fragments;


import android.graphics.Typeface;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.ajkerdeal.app.R;

import NetworkRequest.UserAccess;
import Utility.Validator;

/**
 * A simple {@link Fragment} subclass.
 */
public class LoginFragment extends Fragment implements View.OnClickListener {
    private EditText email;
    private EditText password;

    public LoginFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_login, container, false);
        getActivity().setTitle(R.string.action_login);

        email = (EditText) view.findViewById(R.id.email);
        password = (EditText) view.findViewById(R.id.password);

        Button loginButton = (Button) view.findViewById(R.id.btn_login);

        Typeface typeFace= Typeface.createFromAsset(getActivity().getAssets(),"solaiman-lipi.ttf");
        email.setTypeface(typeFace);
        password.setTypeface(typeFace);
        loginButton.setTypeface(typeFace);

        loginButton.setOnClickListener(this);

        return view;
    }

    public void login(View view) {
        String email = this.email.getText().toString();
        String password = this.password.getText().toString();

        if(!email.isEmpty() && !password.isEmpty() && Validator.isValidEmail(email)) {
            UserAccess networkRequest = new UserAccess();
            networkRequest.Login(this, email, password);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_login:
                login(v);
                break;
        }
    }
}
