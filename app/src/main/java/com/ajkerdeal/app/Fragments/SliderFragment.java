package com.ajkerdeal.app.Fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ajkerdeal.app.R;
import com.daimajia.slider.library.SliderLayout;

import NetworkRequest.BannerRequest;

/**
 * A simple {@link Fragment} subclass.
 */
public class SliderFragment extends Fragment {


    public SliderFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View convertView =  inflater.inflate(R.layout.fragment_slider, container, false);
        SliderLayout imageSlider = (SliderLayout) convertView.findViewById(R.id.imageSlider);
        new BannerRequest().getBanner(this, imageSlider);
        return convertView;
    }

}