package Models;

/**
 * Created by piash on 12/22/15.
 */
public class BannerModel {
    public String ImageLink;
    public int ShowOrder;

    public BannerModel(String imageLink, int showOrder) {
        ImageLink = imageLink;
        ShowOrder = showOrder;
    }

    @Override
    public String toString() {
        return "BannerModel{" +
                "ImageLink='" + ImageLink + '\'' +
                ", ShowOrder=" + ShowOrder +
                '}';
    }
}
