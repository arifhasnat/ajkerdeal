package Models;

/**
 * Created by haider_kazal on 12/14/15.
 */
public class UserDataModel {
    public int Id;
    public String Name;
    public String Email;
    public String Mobile;
    public String Gender;
    public String Address;
    public String Location;
    public String District;

    public UserDataModel(int id, String name, String email, String mobile, String gender,
                         String address, String location, String district) {
        Id = id;
        Name = name;
        Email = email;
        Mobile = mobile;
        Gender = gender;
        Address = address;
        Location = location;
        District = district;
    }

    @Override
    public String toString() {
        return "UserDataModel{" +
                "Id=" + Id +
                ", Name='" + Name + '\'' +
                ", Email='" + Email + '\'' +
                ", Mobile='" + Mobile + '\'' +
                ", Gender='" + Gender + '\'' +
                ", Address='" + Address + '\'' +
                ", Location='" + Location + '\'' +
                ", District='" + District + '\'' +
                '}';
    }
}
