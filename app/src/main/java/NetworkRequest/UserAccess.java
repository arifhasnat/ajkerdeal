package NetworkRequest;

import android.content.Context;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.google.gson.Gson;

import Library.GsonRequest;
import Library.Volley;
import Models.ResetPasswordModel;
import Models.UserDataModel;
import SharedPreference.UserInfo;
import Utility.Urls;

/**
 * Created by haider_kazal on 12/14/15.
 */
public class UserAccess {
    private static final int REQUEST_TIMEOUT = 5000;
    private static final DefaultRetryPolicy retryPolicy = new DefaultRetryPolicy(REQUEST_TIMEOUT,
            DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);

    public void Login(final Fragment fragment, String username, String password) {
        Gson gson = new Gson();

        final String url = Urls.getTestUrl(Urls.UrlType.LOGIN); //Urls.getUrl(Urls.UrlType.LOGIN);
        final String loginPostData = gson.toJson(new LoginPostDataModel(username, password));

        Log.d("UserAccess", loginPostData);

        GsonRequest<UserDataModel> callService = new GsonRequest<>(Request.Method.POST, url,
                UserDataModel.class, null, loginPostData, new Response.Listener<UserDataModel>() {
            @Override
            public void onResponse(UserDataModel response) {
                UserInfo userInfo = UserInfo.getInstance(fragment.getContext());
                userInfo.storeUserInfo(response);
                Snackbar.make(fragment.getView(), "Signed In", Snackbar.LENGTH_LONG).show();
                //Toast.makeText(fragment.getContext(), "Signed In", Toast.LENGTH_LONG).show();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Snackbar errorBar = Snackbar.make(fragment.getView(), "Please Try Again",
                        Snackbar.LENGTH_LONG);
                errorBar.setAction("RETRY", new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                    }
                });
                debug(error);
            }
        });
        callService.setRetryPolicy(retryPolicy);
        Volley.getInstance(fragment.getContext()).addToRequestQueue(callService);
    }

    public void SignUp(final Fragment fragment, SignUpPostDataModel registerData) {
        Gson gson = new Gson();

        String url = Urls.getUrl(Urls.UrlType.SIGNUP);
        String signUpPostData = gson.toJson(registerData);

        GsonRequest<UserDataModel> callService = new GsonRequest<>(Request.Method.POST, url,
                UserDataModel.class, null, signUpPostData, new Response.Listener<UserDataModel>() {
            @Override
            public void onResponse(UserDataModel response) {
                UserInfo userInfo = UserInfo.getInstance(fragment.getContext());
                userInfo.storeUserInfo(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });
        callService.setRetryPolicy(retryPolicy);
        Volley.getInstance(fragment.getContext()).addToRequestQueue(callService);
    }

    public void ResetPassword(final Context context, ResetPasswordPostDataModel resetPasswordData) {
        Gson gson = new Gson();

        String url = Urls.getUrl(Urls.UrlType.RESET_PASSWORD);
        String resetPasswordPostData = gson.toJson(resetPasswordData);

        GsonRequest<ResetPasswordModel> callService = new GsonRequest<>(Request.Method.POST, url,
                ResetPasswordModel.class, null, resetPasswordPostData, new Response.Listener<ResetPasswordModel>() {
            @Override
            public void onResponse(ResetPasswordModel response) {

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });
        callService.setRetryPolicy(retryPolicy);
        Volley.getInstance(context).addToRequestQueue(callService);
    }

    private static void debug(VolleyError error) {
        if(error != null) {
            Log.e("UserAccess", error.getStackTrace().toString());
            Log.e("UserAccess", "" + error.getNetworkTimeMs());
            //Log.e("UserAccess", error.getLocalizedMessage());
            //Log.e("UserAccess", error.getMessage());
            if(error.networkResponse != null) {
                Log.e("UserAccess", error.networkResponse.toString());
                Log.e("UserAccess", "" + error.networkResponse.statusCode);
            }
        }
    }

    public class LoginPostDataModel {
        public String Email;
        public String Password;

        public LoginPostDataModel(String email, String password) {
            Email = email;
            Password = password;
        }
    }

    public class SignUpPostDataModel {
        public String Name;
        public String Email;
        public String Password;
        public String Mobile;
        public String Gender;
        public String Address;
        public int LocationId;
        public int DistrictId;
    }

    public class ResetPasswordPostDataModel {
        public String Email;
    }
}
