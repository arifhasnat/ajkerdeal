package NetworkRequest;

import android.support.v4.app.Fragment;
import android.util.Log;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.SliderTypes.DefaultSliderView;

import java.util.HashMap;

import Library.GsonRequest;
import Library.Volley;
import Models.BannerModel;
import Utility.Urls;

/**
 * Created by piash on 12/22/15.
 */
public class BannerRequest {
    private static final int REQUEST_TIMEOUT = 5000;
    private static final DefaultRetryPolicy retryPolicy = new DefaultRetryPolicy(REQUEST_TIMEOUT,
            DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);

    public void getBanner(final Fragment fragment, final SliderLayout imageSlider) {
        String url = Urls.getTestUrl(Urls.UrlType.BANNER);

        GsonRequest<BannerModel[]> callService = new GsonRequest<>(Request.Method.GET, url,
                BannerModel[].class, null, null, new Response.Listener<BannerModel[]>() {
            @Override
            public void onResponse(BannerModel[] response) {
                HashMap<String, String> urlMaps = new HashMap<>();
                for (BannerModel banner : response) {
                    String imageName = "Banner" + banner.ShowOrder;
                    urlMaps.put(imageName, banner.ImageLink);
                }

                for (String keyName: urlMaps.keySet()) {
                    DefaultSliderView sliderView = new DefaultSliderView(fragment.getActivity());

                    sliderView.description(keyName)
                            .image(urlMaps.get(keyName))
                            .setScaleType(BaseSliderView.ScaleType.Fit);
                    imageSlider.addSlider(sliderView);
                    //imageSlider.setCustomIndicator((PagerIndicator) convertView.findViewById(R.id.indicator));
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                if(error != null) {
                    Log.e("Banner", error.getStackTrace().toString());
                    Log.e("Banner", "" + error.getNetworkTimeMs());
                    //Log.e("UserAccess", error.getLocalizedMessage());
                    //Log.e("UserAccess", error.getMessage());
                    if(error.networkResponse != null) {
                        Log.e("Banner", error.networkResponse.toString());
                        Log.e("Banner", "" + error.networkResponse.statusCode);
                    }
                }

                //Log.e("UserAccess", "" + error.networkResponse.statusCode);

            }
        });
        callService.setRetryPolicy(retryPolicy);
        Volley.getInstance(fragment.getContext()).addToRequestQueue(callService);
    }
}
